#pragma once
#include "stdafx.h"
#include "MatrixBiDimensional.h"
template<typename T>
MatrixBiDimensional<T>::MatrixBiDimensional() {
	throw new Exception();
}
template<typename T>
MatrixBiDimensional<T>::MatrixBiDimensional(std::initializer_list<std::initializer_list<T>> values)
{
	m_rowCount = values.size();
	m_columnCount = values.begin()->size();
	m_matrixPointer = std::unique_ptr<T[]>(new T[m_rowCount * m_columnCount]);
	size_t i = 0,
		j = 0;
	size_t aa;
	for (const auto& row : values)
	{
		for (const auto& v : row)
		{
			m_matrixPointer[i * m_columnCount + j] = v;
			j++;
		}
		j = 0;
		i++;
	}
}
template<typename T>
MatrixBiDimensional<T>::MatrixBiDimensional(const size_t rowCount, const size_t columnCount)
{
	m_rowCount = rowCount;
	m_columnCount = columnCount;
	m_matrixPointer = std::unique_ptr<T[]>(new T[m_rowCount * m_columnCount]);
}

template<typename T>
T MatrixBiDimensional<T>::Get(size_t rowIndex, size_t columnIndex)
{
	return m_matrixPointer[rowIndex * m_columnCount + columnIndex];
}

template<typename T> 
void MatrixBiDimensional<T>::Set(size_t rowIndex, size_t columnIndex, T value)
{
	m_matrixPointer[rowIndex * m_columnCount + columnIndex] = value;
}

template<typename T>
size_t MatrixBiDimensional<T>::RowNumber() {
	return m_rowCount;
}

template<typename T>
size_t MatrixBiDimensional<T>::ColumnNumber() {
	return m_columnCount;
}

