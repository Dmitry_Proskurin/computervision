#include "stdafx.h"
#include "CopyEdges.h"

double CopyEdges::Resolve(int i, int j, const MatrixBiDimensional<double> &image) const
{
	int newI=i;
	int newJ=j;
	if (i < 0)
		newI = 0;
	else if (i >= image.RowNumber())
		newI = image.RowNumber() - 1;
	if (j < 0)
		newJ = 0;
	else if (j >= image.ColumnNumber())
		newJ = image.ColumnNumber() - 1;
	return image.Get(newI, newJ);
}
CopyEdges::CopyEdges()
{
}


CopyEdges::~CopyEdges()
{
}
