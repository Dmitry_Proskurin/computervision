#pragma once
#include "stdafx.h"
#include "Convolution.h"
#include "MatrixBiDimensional.h"
#include <vector>
using namespace std;
Convolution::Convolution(std::shared_ptr<IEdgeBehaviour> &edgeResolver)
{
	m_edgeResolver = edgeResolver;
}


Convolution::~Convolution()
{
}

MatrixBiDimensional<double> Convolution::Transform(const MatrixBiDimensional<double> &image, const vector<double> &row, const vector<double> &column)
{
	const auto rowsNumber = image.RowNumber();
	const auto columnNumber = image.ColumnNumber();
	auto result = MatrixBiDimensional<double>(rowsNumber, columnNumber);
	const auto halfRow = (int)(row.size() - 1) / 2;
	const auto halfColumn = (int)(column.size() - 1) / 2;
	auto buff = new double[row.size()];
	//������� �����
	for (int i = 0; i < rowsNumber; i++) { //����������� �����
		auto upperBoundVertical = i + halfColumn == rowsNumber ? i + halfColumn - 1 : i + halfColumn;
		for (int j = 0; j < columnNumber; j++) {
			double summ = 0;
			auto upperBoundHorizontal = j + halfRow == columnNumber ? j + halfRow - 1 : j + halfRow;
			for (int horizontal = i - halfColumn, buffIndex = 0; horizontal <= upperBoundVertical; horizontal++, buffIndex++) {
				for (int vertical = j - halfRow, filterIndex = 0; vertical <= upperBoundHorizontal; vertical++, filterIndex++) {
					if (horizontal < 0 ||
						vertical < 0 ||
						horizontal >= rowsNumber ||
						vertical >= columnNumber)
					{
						summ += (*m_edgeResolver).Resolve(horizontal, vertical, image)*row[filterIndex];
					}
					else
					{
						summ += image.Get(horizontal, vertical)*row[filterIndex];
					}
				}
				buff[buffIndex] = summ;
				summ = 0;
			}
			summ = 0;
			for (int vertical = 0; vertical < column.size(); vertical++) {
				summ += buff[vertical] * column[vertical];
			}
			result.Set(i, j, summ);
		}
	}
	return result;
}
MatrixBiDimensional<double> Convolution::Transform(const MatrixBiDimensional<double> &image, const MatrixBiDimensional<double> &kernel)
{
	auto rowsNumber = image.RowNumber();
	auto columnNumber = image.ColumnNumber();
	auto result = MatrixBiDimensional<double>(rowsNumber, columnNumber);
	int halfCoreWidth = (kernel.ColumnNumber() - 1) / 2;
	int halfCoreHeight = (kernel.RowNumber() - 1) / 2;
	//������� �����
	for (int i = 0; i < rowsNumber; i++) {
		auto upperBoundVertical = i + halfCoreWidth;
		if (i == rowsNumber - 1)
		{
			int a = 0;
		}
		for (int j = 0; j < columnNumber; j++){
			double summ = 0;
			auto upperBoundHorizontal = j + halfCoreHeight;
			for (int horizontal = i - halfCoreWidth, filterHorizontal = 0; horizontal <= upperBoundVertical; horizontal++, filterHorizontal++) {
				for (int vertical = j - halfCoreHeight, filterVertical = 0; vertical <= upperBoundHorizontal; vertical++, filterVertical++) {
					if (horizontal < 0 ||
						vertical < 0 ||
						horizontal >= rowsNumber ||
						vertical >= columnNumber)
					{
						auto res = (*m_edgeResolver).Resolve(horizontal, vertical, image);
						auto kern = kernel.Get(filterHorizontal, filterVertical);
						summ+=res*kern;
					}
					else
					{
						auto res = image.Get(horizontal, vertical);
						auto kern = kernel.Get(filterHorizontal, filterVertical);
						summ += res*kern;
					}
				}
			}
			result.Set(i,j, summ);
		}
	}
	return result;
}