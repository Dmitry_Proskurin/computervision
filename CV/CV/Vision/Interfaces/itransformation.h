#pragma once
#include "../../MatrixBiDimensional.h"
#include <memory>
#include <vector>
#include <array>
using namespace std;
class ITransformation{
public:
	virtual MatrixBiDimensional<double> Transform(const MatrixBiDimensional<double> &image, const vector<double> &row, const vector<double> &column)=0;
	virtual MatrixBiDimensional<double> Transform(const MatrixBiDimensional<double> &image, const MatrixBiDimensional<double> &kernel) = 0;
};


