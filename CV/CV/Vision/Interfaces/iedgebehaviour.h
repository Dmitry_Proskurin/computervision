#pragma once
#include "../../MatrixBiDimensional.h"
#include <memory>
using namespace std;
class IEdgeBehaviour {
public:
	virtual double Resolve(int i, int j, const MatrixBiDimensional<double> &image) const = 0;
};
