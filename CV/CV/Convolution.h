#pragma once
#include "Vision\Interfaces\iedgebehaviour.h"
#include "Vision\Interfaces\itransformation.h"
class Convolution:ITransformation
{
private:
	std::shared_ptr<IEdgeBehaviour> m_edgeResolver;
public:
	virtual MatrixBiDimensional<double> ITransformation::Transform(const MatrixBiDimensional<double> &image, const vector<double> &row, const vector<double> &column) override;
	virtual MatrixBiDimensional<double> ITransformation::Transform(const MatrixBiDimensional<double> &image, const MatrixBiDimensional<double> &kernel) override;

	Convolution(std::shared_ptr<IEdgeBehaviour> &edgeResolver);
	~Convolution();
};

