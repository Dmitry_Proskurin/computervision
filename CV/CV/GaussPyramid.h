#pragma once
#include "MatrixBiDimensional.h"
#include <vector>
#include "Gauss.h"
#include "Vision\Interfaces\iedgebehaviour.h"
#include "Convolution.h"
#include "Extensions\extensions.h"
#include <math.h>
#include "CopyEdges.h"
#include <memory>
using namespace std;
namespace GaussPyramid
{
	static vector<pair<MatrixBiDimensional<double>, double>> CalculatePyramid(const MatrixBiDimensional<double> &image, const double initialSigma, const int scaleNumber, const int octaveCount)
	{
		const double sigmaStep = pow(2, 1.0 / (double)scaleNumber);
		auto edgeResolver = shared_ptr<IEdgeBehaviour>(new CopyEdges());
		auto transformation = new Convolution(edgeResolver);
		auto result = vector<pair<MatrixBiDimensional<double>, double>>();
		int counter = 1;
		for (int octave = 0; octave < octaveCount; octave++)
		{
			for (int scale = 0; scale < scaleNumber; scale++) {
				if (octave == 0 && scale == 0) {
					auto gaussFilter = Gauss::CalculateMatrixSeparate(0.6);
					{
						double summ = 0;
						for (auto i = 0; i < gaussFilter.first.size(); i++)
							summ += gaussFilter.first[i];
						for (auto i = 0; i < gaussFilter.first.size(); i++)
						{
							double elem = gaussFilter.first[i];
							gaussFilter.second[i] = gaussFilter.first[i] = elem / summ;
						}
					}
					auto buffImage = transformation->Transform(image, gaussFilter.first, gaussFilter.second);
					result.push_back({ buffImage, 0.6 });
					continue;
				}
				const auto &previous = result.back();
				if (scale != 0) {
					const auto currentSigma = initialSigma*pow(sigmaStep, scale);
					auto gaussFilter = Gauss::CalculateMatrixSeparate(sqrt(abs(currentSigma * currentSigma - initialSigma*pow(sigmaStep, scale - 1))));
					{
						double summ = 0;
						for (auto i = 0; i < gaussFilter.first.size(); i++)
							summ += gaussFilter.first[i];
						for (auto i = 0; i < gaussFilter.first.size(); i++)
						{
							double elem = gaussFilter.first[i];
							gaussFilter.second[i] = gaussFilter.first[i] = elem / summ;
						}
					}
					auto buffImage = transformation->Transform(previous.first, gaussFilter.first, gaussFilter.second);
					result.push_back({ buffImage, initialSigma*pow(sigmaStep,counter++) });
				}
				else {
					auto aaa = previous.second;
					result.emplace_back(Extensions::DownSample(previous.first), aaa);
				}
			}
		}
		return result;
	}

};

