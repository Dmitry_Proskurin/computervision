#pragma once
#include "stdafx.h"
#include "MatrixBiDimensional.h"
#include <vector>
#include <cstdlib>
#include <vector>
#include <tuple>
#include <memory>
#include <math.h>
using namespace std;
namespace Gauss
{
	static MatrixBiDimensional<double> CalculateMatrixFull(const double sigma)
	{
		auto matrixSize = (int)(3 * sigma) + 1;
		if (matrixSize % 2 == 0)
			matrixSize++;
		auto result = MatrixBiDimensional<double>(matrixSize, matrixSize);
		for (int i = -matrixSize / 2, rowIndex = 0; rowIndex < matrixSize; i++, rowIndex++) {
			for (int j = -matrixSize / 2, columnIndex = 0; columnIndex < matrixSize; j++, columnIndex++) {
				result.Set(rowIndex, columnIndex, (1.0 / (2.0 * PI *sigma*sigma)*exp(-((double)(i*i + j*j)) / (2.0 * sigma*sigma))));
			}
		}
		return result;
	}

	static pair<vector<double>, vector<double>> CalculateMatrixSeparate(const double sigma)
	{
		const double sqrt2PI = sqrt(PI + PI);
		int matrixSize = (int)(3 * sigma) + 1;
		if (matrixSize % 2 == 0)
			matrixSize++;
		auto result = make_pair<vector<double>, vector<double>>(vector<double>(matrixSize), vector<double>(matrixSize));
		for (int i = -matrixSize / 2, rowIndex = 0; rowIndex < matrixSize; i++, rowIndex++) {
			const auto prom = 1.0 / (sqrt2PI * sigma) * exp(-((double)(i*i)) / (2 * sigma * sigma));
			result.first[rowIndex] = prom;
			result.second[rowIndex] = prom;
		}
		return result;
	}
};

