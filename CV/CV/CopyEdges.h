#pragma once
#include "Vision\Interfaces\iedgebehaviour.h"
class CopyEdges :
	public IEdgeBehaviour
{
public:
	virtual double IEdgeBehaviour::Resolve(int i, int j, const MatrixBiDimensional<double> &image) const override;
	CopyEdges();
	~CopyEdges();
};

