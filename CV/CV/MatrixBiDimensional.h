#pragma once
#include <cstdlib>
#include <memory>
using namespace std;

template<typename T>
class MatrixBiDimensional
{
private:
	size_t m_rowCount;
	size_t m_columnCount;
	unique_ptr<T[]> m_matrixPointer;
	MatrixBiDimensional<T>();
public:
	const MatrixBiDimensional<T> & operator = (MatrixBiDimensional<T> && _old)
	{
		m_rowCount = _old.m_rowCount;
		m_columnCount = _old.m_columnCount;
		m_matrixPointer = make_unique<T[]>(m_rowCount*m_columnCount);
		for (int i = 0; i < m_rowCount*m_columnCount; i++)
			m_matrixPointer[i] = _old.m_matrixPointer[i];
		return *this;
	}
	MatrixBiDimensional<T>(initializer_list<initializer_list<T>> values) {
		m_rowCount = values.size();
		m_columnCount = values.begin()->size();
		m_matrixPointer = make_unique<T[]>(m_rowCount * m_columnCount);
		int i = 0,
			j = 0;
		int aa;
		for (const auto& row : values)
		{
			for (const auto& v : row)
			{
				m_matrixPointer[i * m_columnCount + j] = v;
				j++;
			}
			j = 0;
			i++;
		}
	}
	
	MatrixBiDimensional<T>(const int rowCount, const int columnCount)
	{
		m_rowCount = rowCount;
		m_columnCount = columnCount;
		m_matrixPointer = make_unique<T[]>(m_rowCount * m_columnCount);
	}
	
	MatrixBiDimensional<T>(MatrixBiDimensional<T> &&matrix) = default;

	MatrixBiDimensional<T>(const MatrixBiDimensional<T> &matrix) {
		m_rowCount = matrix.RowNumber();
		m_columnCount = matrix.ColumnNumber();
		m_matrixPointer = make_unique<T[]>(m_rowCount * m_columnCount);
		for (auto i = 0; i < m_rowCount*m_columnCount; i++) {
			m_matrixPointer[i] = matrix.m_matrixPointer[i];
		}
	}

	

	T Get(int rowIndex, int columnIndex) const
	{
		return m_matrixPointer[rowIndex * m_columnCount + columnIndex];
	}

	void Set(int rowIndex, int columnIndex, T value)
	{
		m_matrixPointer[rowIndex * m_columnCount + columnIndex] = value;
	}

	void Set(T value) {
		fill(m_matrixPointer.get(), m_matrixPointer.get() + m_rowCount*m_columnCount, value);
	}

	pair<T, T> MinMax() const
	{
		auto result = minmax_element(m_matrixPointer.get(), m_matrixPointer.get() + m_columnCount*m_rowCount);
		return{ *result.first, *result.second };
	}

	int RowNumber() const
	{
		return m_rowCount;
	}

	int ColumnNumber() const
	{
		return m_columnCount;
	}
};

