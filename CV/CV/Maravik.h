#pragma once
#include "Vision\Interfaces\iedgebehaviour.h"
#include "MatrixBiDimensional.h"
#include <assert.h>
#include <vector>
#include <memory>
#include <cstdlib>

using namespace std;
class Maravik
{
private:
	int m_windowSizeX;
	int m_windowSizeY;
	shared_ptr<IEdgeBehaviour> m_edgeBehaviour;
public:
	Maravik(int windowSizeX,
			int windowSizeY);
	void SetWindowsSize(int sizeX, int sizeY);
	pair<const int, const int> GetWindowSize() const;

	MatrixBiDimensional<double> CalculateDifference(MatrixBiDimensional<double> &image) const;
	~Maravik();
};

