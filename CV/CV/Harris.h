#pragma once
#include "MatrixBiDimensional.h"
#include <tuple>
#include <vector>
class Harris
{
private:
	int m_windowX;
	int m_windowY;
public:
	Harris(int windowX, int windowY);
	MatrixBiDimensional<double> CalculateHarris(const MatrixBiDimensional<double> &image) const;
	static vector<tuple<double, int, int>> FilterPoints(MatrixBiDimensional<double> &operatorValues, double edge, int wHeight, int wWidth);
	static void ANMS(vector<tuple<double, int, int>> &points, int maxPoints, int pictureWidth, int pictureHeight);
	~Harris();
};

