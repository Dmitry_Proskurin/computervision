#pragma once
#include "stdafx.h"
#include <vector>
#include "MatrixBiDimensional.h"
#include "Extensions/extensions.h"
#include "GoH.h"
using namespace System;
namespace Course
{
	
	static void Match(vector<pair<int,int>> &originalPoints, vector<pair<int,int>> &rotatedPoints,
						vector<tuple<int,int,double,vector<double>>> &descr1, vector<tuple<int,int,double,vector<double>>> &descr2) {
		//����� ���������� � ������������
		for (int i = 0; i < originalPoints.size(); i++) {
			auto original = originalPoints[i];//���������� ������������ �����
			auto rotated = rotatedPoints[i];//���������� ��������� �����
			vector<tuple<int, int, double, vector<double>>> originalDescr;
			vector<tuple<int, int, double, vector<double>>> rotatedDescr;
			auto flag = true;
			for (int j = 0; j < descr1.size() && flag; j++) {
				if (get<0>(descr1[j]) == original.first && get<1>(descr1[j]) == original.second) {
					//��� ������� �����������
					originalDescr.push_back(descr1[j]);
					if (j + 1 < descr1.size() && get<0>(descr1[j + 1]) == original.first && get<1>(descr1[j + 1]) == original.second) {
						originalDescr.push_back(descr1[j + 1]);
					}
					flag = false;
				}
			}
			flag = true;
			//��� ����������
			for (int j = 0; j < descr2.size() && flag; j++) {
				if (get<0>(descr2[j]) == rotated.first && get<1>(descr2[j]) == rotated.second) {
					//��� ������� �����������
					rotatedDescr.push_back(descr2[j]);
					if (j + 1 < descr2.size() && get<0>(descr2[j + 1]) == rotated.first && get<1>(descr2[j + 1]) == rotated.second) {
						rotatedDescr.push_back(descr2[j + 1]);
					}
					flag = false;
				}
			}
			for (auto ii : originalDescr) {
				for (auto jj : rotatedDescr) {
					double dist = GoH::DistanceBetweenDescriptors(ii, jj);
					auto angle = abs(get<2>(ii) * 180 / PI - get<2>(jj) * 180 / PI);
					Console::WriteLine(String::Format("��������(y,x, ����) ({0,4}, {1,4}, {2,6:N3}); ��������� �����������(y,x, ����) ({3,4}, {4,4}, {5,6:N3}); distance: {6,6:N3}",
						get<0>(ii), get<1>(ii), get<2>(ii)*180/PI, get<0>(jj), get<1>(jj), get<2>(jj)*180/PI, dist));
				}
			}
		}
	}
	static void MatchDescriptors(vector<tuple<int, int, double, vector<double>>> descr1, vector<tuple<int, int, double, vector<double>>> descr2,
		Graphics ^graphForDrawing, int xTransform)
	{
		auto brush = gcnew SolidBrush(Color::Red);
		for (int i=0;i<descr1.size();i++)
		{
			double minDistanceFirst = -1, minDistanceSecond=-1, distance;
			int firstMinIndex, secondMinIndex;
			for (int j=0;j<descr2.size();j++)
			{
				distance = GoH::DistanceBetweenDescriptors(descr1[i], descr2[j]);
				if (minDistanceFirst < 0) {
					minDistanceFirst = distance;
					firstMinIndex = j;
				}
				else {
					if (minDistanceFirst > distance) {

						minDistanceSecond = minDistanceFirst;
						secondMinIndex = firstMinIndex;

						minDistanceFirst = distance;
						firstMinIndex = j;
					}
					else if (minDistanceSecond < 0 || minDistanceSecond > distance) {
						minDistanceSecond = distance;
						secondMinIndex = j;
					}
				}
			}
			if(minDistanceFirst/minDistanceSecond < 0.9)
			{
				graphForDrawing->DrawLine(gcnew Pen(Color::Red), get<1>(descr1[i]), get<0>(descr1[i]), get<1>(descr2[firstMinIndex]) + xTransform, get<0>(descr2[firstMinIndex]));
				graphForDrawing->FillRectangle(brush, get<1>(descr1[i]) - 2, get<0>(descr1[i]) - 2, 4, 4);
				graphForDrawing->FillRectangle(brush, get<1>(descr2[firstMinIndex]) + xTransform - 2, get<0>(descr2[firstMinIndex]) - 2, 4, 4);
			}
		}
	}
	static void MainCourse(int anmsValue, int harrisUp, bool match, double step, String ^fileName, double maxAngle) {
		Image ^img = Image::FromFile(fileName);
		auto bitmap = gcnew Bitmap(img);

		auto grays = Extensions::GetGrayCoeff(bitmap);
		////////////////////////////////////////////////
		//1 image
		auto harris = Harris(7, 7);
		auto points1 = harris.CalculateHarris(grays);
		auto filteredPoints = Harris::FilterPoints(points1, harrisUp, 7, 7);
		Harris::ANMS(filteredPoints, anmsValue, grays.RowNumber(), grays.ColumnNumber());
		auto originalPoints = vector<pair<int, int>>();
		
		for (auto ii : filteredPoints)
			if (!isnan(get<0>(ii)))
			{
				originalPoints.emplace_back(get<1>(ii), get<2>(ii));
			}
		auto withAngle = GoH::CalculateDirection(grays, originalPoints, 16, 1, 36);
		auto descr1 = vector<tuple<int, int, double, vector<double>>>();
		auto descr = vector<tuple<int, int, double, vector<double>>>();
		auto edgeResolver = new CopyEdges();
		auto edgeResolver1 = shared_ptr<IEdgeBehaviour>(edgeResolver);
		auto transformation = new Convolution(edgeResolver1);
		const auto sobelXgrays = transformation->Transform(grays, sobelX);
		const auto sobelYgrays = transformation->Transform(grays, sobelY);
		for (auto ii : withAngle) {
			descr1.push_back(GoH::MakeGOHFromImage(sobelXgrays, sobelYgrays, pair<int, int>(get<0>(ii), get<1>(ii)), 16, 4, get<2>(ii)));
		}
		Extensions::DrawPoints(bitmap, originalPoints);
		Extensions::SaveImage(bitmap, "CourseOriginalPoints.png");
		Console::WriteLine("Original image processed.");
		for (double angle = step; angle < maxAngle; angle += step)
		{
			auto originalPointsForFiltration = vector<pair<int, int>>();
			Console::WriteLine("Starting process image for angle {0,5:N3}", angle);
			auto rotatedPoints = vector<pair<int, int>>();
			for (int j = 0; j < originalPoints.size(); j++) {
				double newY = originalPoints[j].first - bitmap->Height / 2;
				double newX = originalPoints[j].second - bitmap->Width / 2;
				//��������� ������� ���������
				{
					const double radAngle = step*PI / 180;
					const double  rotatedX = newX*cos(radAngle) - newY*sin(radAngle);
					newY = newX*sin(radAngle) + newY*cos(radAngle);
					newX = rotatedX;
				}

				const int rotatedWindowY = newY + bitmap->Height / 2;
				const int rotatedWindowX = newX + bitmap->Width / 2;
				if (rotatedWindowY < 0 || rotatedWindowY >= bitmap->Height ||
					rotatedWindowX < 0 || rotatedWindowX >= bitmap->Width) {
					continue;
				}
				else {
					originalPointsForFiltration.emplace_back(originalPoints[j].first, originalPoints[j].second);
					rotatedPoints.emplace_back(rotatedWindowY, rotatedWindowX);
				}
			}
			auto bitmapRotated = gcnew Bitmap(bitmap->Width, bitmap->Height);
			auto bitmapRotatedGraph = Graphics::FromImage(bitmapRotated);
			bitmapRotatedGraph->InterpolationMode = System::Drawing::Drawing2D::InterpolationMode::HighQualityBicubic;
			bitmapRotatedGraph->TranslateTransform((float)bitmap->Width / 2, (float)bitmap->Height / 2);
			bitmapRotatedGraph->RotateTransform(angle);
			bitmapRotatedGraph->TranslateTransform(-(float)bitmap->Width / 2, -(float)bitmap->Height / 2);
			bitmapRotatedGraph->DrawImage(img, 0, 0, bitmap->Width, bitmap->Height);
			bitmapRotatedGraph->ResetTransform();

			auto grays2 = Extensions::GetGrayCoeff(bitmapRotated);
			auto withAngle2 = GoH::CalculateDirection(grays2, rotatedPoints, 16, 1, 36);
			auto descr2 = vector<tuple<int, int, double, vector<double>>>();
			const auto sobelXgrays1 = transformation->Transform(grays2, sobelX);
			const auto sobelYgrays1 = transformation->Transform(grays2, sobelY);
			for (auto ii : withAngle2) {
				descr2.push_back(GoH::MakeGOHFromImage(sobelXgrays1, sobelYgrays1, pair<int, int>(get<0>(ii), get<1>(ii)), 16, 4, get<2>(ii)));
			}

			Bitmap ^bb = gcnew Bitmap(bitmap->Width + 100 + bitmapRotated->Width, bitmap->Height > bitmapRotated->Height ? bitmap->Height : bitmapRotated->Height);
			auto graphForDrawing = Graphics::FromImage(bb);
			const int xTransform = bitmap->Width + 100;
			graphForDrawing->DrawImage(bitmap, 0, 0);
			graphForDrawing->DrawImage(bitmapRotated, xTransform, 0);
			//����� ���������� � ������������
			Course::Match(originalPointsForFiltration, rotatedPoints, descr1, descr2);
			//������������� � ���������
			if (match)
			{
				Course::MatchDescriptors(descr1, descr2, graphForDrawing, xTransform);
			}
			Extensions::SaveImage(bb, String::Format("rotatedCourse_{0,5:N3}.jpg",angle));
			Extensions::DrawPoints(bitmapRotated, rotatedPoints);
			Extensions::SaveImage(bitmapRotated, String::Format("CourseRotatedPoints_{0,5:N3}.png", angle));
			Console::WriteLine("End process image for angle {0,5:N3}, all images saved.", angle);
		}

		Console::WriteLine("Ready");
	}
};

