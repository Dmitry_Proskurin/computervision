#pragma once
#include "stdafx.h"
#include "Harris.h"
#include <functional>
#include "Convolution.h"
#include "Gauss.h"
#include "CopyEdges.h"
#include <tuple>
#include "Extensions\extensions.h"
using namespace System;

Harris::Harris(const int windowX, const int windowY)
{
	m_windowX = windowX;
	m_windowY = windowY;
}

MatrixBiDimensional<double> Harris::CalculateHarris(const MatrixBiDimensional<double>& image) const
{
	auto edgeResolver = shared_ptr<IEdgeBehaviour>(new CopyEdges());
	auto transformation = new Convolution(edgeResolver);
	auto sobelXImg = transformation->Transform(image, sobelX);
	auto sobelYImg = transformation->Transform(image, sobelY);
	const auto rowNumber = image.RowNumber();
	const auto columnNumber = image.ColumnNumber();
	auto points = MatrixBiDimensional<double>(image.RowNumber(), image.ColumnNumber());
	const auto halfWndHeight = m_windowY / 2;
	const auto halfWndWidth = m_windowX / 2;
	points.Set(0);
	auto gaussMatrix = MatrixBiDimensional<double>(m_windowY, m_windowX);
	for (int i = -halfWndHeight, rowIndex = 0; rowIndex < m_windowY; i++, rowIndex++) {
		for (int j = -halfWndWidth, columnIndex = 0; columnIndex < m_windowX; j++, columnIndex++) {
			gaussMatrix.Set(rowIndex, columnIndex, (1.0 / (2.0 * PI)*exp(-((double)(i*i + j*j)) / (2.0))));
		}
	}
	//������������
	auto summ1 = 0.0;
	for (int i = 0; i < gaussMatrix.RowNumber(); i++) {
		for (int j = 0; j < gaussMatrix.ColumnNumber(); j++) {
			summ1 += gaussMatrix.Get(i, j);
		}
	}
	for (int i = 0; i < gaussMatrix.RowNumber(); i++) {
		for (int j = 0; j < gaussMatrix.ColumnNumber(); j++) {
			gaussMatrix.Set(i, j, gaussMatrix.Get(i, j) / summ1);
		}
	}
	//
	for (auto i = halfWndHeight; i < rowNumber - halfWndHeight; i++) { //����� �� ����������� ����� ���� �����������
		for (auto j = halfWndWidth; j < columnNumber - halfWndWidth; j++) {
			auto A = 0.0,
				C = 0.0,
				B = 0.0,
				sobelXBuff = 0.0,
				sobelYBuff = 0.0;
			for (auto k = -halfWndHeight, kk=0; k <= halfWndHeight; k++,kk++) {
				for (auto m = -halfWndWidth, mm=0; m <= halfWndWidth; m++,mm++) {
					sobelXBuff = sobelXImg.Get(i + k, j + m) *gaussMatrix.Get(kk, mm);
					sobelYBuff = sobelYImg.Get(i + k, j + m) *gaussMatrix.Get(kk, mm);
					A += sobelXBuff*sobelXBuff;//A
					C += sobelYBuff*sobelYBuff;//C
					B += sobelXBuff*sobelYBuff;//B
				}
			}
			auto kk = A*C - B*B - 0.06*(A + C)*(A + C);
			points.Set(i, j, kk);
		}
	}
	return points;
}

vector<tuple<double, int, int >> Harris::FilterPoints(MatrixBiDimensional<double>& operatorValues, double edge, int wHeight, int wWidth)
{
	auto vectorPoints = vector<tuple<double, int, int>>();
	const auto rowNumber = operatorValues.RowNumber();
	const auto columnNumber = operatorValues.ColumnNumber();
	const auto halfWndHeight = wHeight / 2;
	const auto halfWndWidth = wWidth / 2;
	//����������� ����� � �����������+���������� �� �������
	for (int i = halfWndHeight; i < rowNumber - halfWndHeight; i++) {
		for (int j = halfWndWidth; j < columnNumber - halfWndHeight; j++) {
			//����� ������������ � ����
			double max = operatorValues.Get(i, j);
			if (max < edge)
				continue;
			const auto rowUpperBound = i + halfWndHeight + 1 > rowNumber ? rowNumber : i + halfWndHeight + 1;
			const auto columnUpperBound = j + halfWndWidth + 1 > columnNumber ? columnNumber : j + halfWndWidth + 1;
			auto flag = true;
			for (int horizontal = i - halfWndHeight; (horizontal < rowUpperBound) && flag; horizontal++) {
				for (int vertical = j - halfWndWidth; (vertical < columnUpperBound) && flag; vertical++) {
					if (horizontal == i && vertical == j)
						continue;
					const double buff = operatorValues.Get(horizontal, vertical);
					if (max <= buff) {
						flag = false;
					}
				}
			}
			if (flag)
			{
				vectorPoints.emplace_back(max, i, j);
			}
		}
	}
	return vectorPoints;
}

void Harris::ANMS(vector<tuple<double, int, int>>& points,const int maxPoints, const int pictureWidth, const int pictureHeight)
{
	int radius = 0;
	const int maxRadius =sqrt(pictureWidth*pictureWidth+pictureHeight*pictureHeight);
	auto countElements = points.size();
	while (countElements > maxPoints && radius <= maxRadius) {
		for (int i = 0; i < points.size(); i++) {
			double intensOuter = get<0>(points[i]);
			if (isnan(intensOuter))
				continue;
			for (int j = i + 1; j < points.size(); j++) {
				if (!isnan(get<0>(points[i])))
				{
					double distance = hypot(get<1>(points[i]) - get<1>(points[j]),get<2>(points[i]) - get<2>(points[j]));
					double intensInner = 0.9 * get<0>(points[j]);
					if (distance <= radius && intensOuter < intensInner) {
						get<0>(points[i]) = NAN;
						countElements--;
						break;
					}
				}
			}
		}
		radius++;
	}
}

Harris::~Harris()
{
}
