#pragma once
#include "stdafx.h"
#include <vector>
#include "MatrixBiDimensional.h"
#include <assert.h>
#include "Vision/Interfaces/iedgebehaviour.h"
#include "Extensions/extensions.h"
#include "Convolution.h"
#include <functional>
#include <numeric>
#include "CopyEdges.h"
using namespace System;
namespace GoH {

	static double DistanceBetweenDescriptors(tuple<int, int,double, vector<double>> &first, tuple<int, int, double, vector<double>> &second) {
		const auto firstVector = get<3>(first),
					secondVector = get<3>(second);
		double summ = 0;
		for (int i = 0; i < firstVector.size(); i++) {
			summ += (firstVector[i] - secondVector[i])*(firstVector[i] - secondVector[i]);
		}
		return sqrt(summ);
	}

	static vector<tuple<int, int, double>> CalculateDirection(const MatrixBiDimensional<double> &image,
		vector<pair<int, int>> &pointsOfInterest,
		const int windowSize, const double imageScale, const int binCount, bool isDirect = true) {
		auto edgeResolver = new CopyEdges();
		auto edgeResolver1 = shared_ptr<IEdgeBehaviour>(edgeResolver);
		auto transformation = new Convolution(edgeResolver1);
		const auto sobelXdiff = transformation->Transform(image, sobelX);
		const auto sobelYdiff = transformation->Transform(image, sobelY);
		const auto halfBin = PI / binCount;
		const auto fullBinAngle = halfBin * 2;
		const auto halfWindow = windowSize / 2;
		auto result = vector<tuple<int, int, double>>();
		for (auto ii : pointsOfInterest) {
			auto discriptor = vector<double>(binCount);
			for (int subwindowRow = -halfWindow; subwindowRow < halfWindow; subwindowRow++) {
				for (int subWindowColumn = -halfWindow; subWindowColumn < halfWindow; subWindowColumn++) {
					const double pictureY = ii.first + subwindowRow,
						pictureX = ii.second + subWindowColumn;
					if (pictureX < 0 || pictureX >= image.ColumnNumber())
						continue;
					if (pictureY < 0 || pictureY >= image.RowNumber())
						continue;
					const double tmpX = sobelXdiff.Get(pictureY, pictureX),
								tmpY = sobelYdiff.Get(pictureY, pictureX);
					const auto gradient = hypot(tmpX, tmpY);
					auto radAngle = atan2(tmpY, tmpX) + PI;
					if (radAngle > 2 * PI)
						radAngle -= 2 * PI;
					if (radAngle < 0)
						radAngle += 2 * PI;

					double part = radAngle / PI / 2 * binCount;
					part = max(0.0, min(binCount - 0.001, part));

					const double partVariation = part - (int)part;
					if (radAngle < halfBin) {//������� �� 0 ������� � ���������
						discriptor[0] += gradient*(1 - partVariation);
						discriptor[binCount - 1] += gradient*partVariation;
					}
					else if (radAngle > 2 * PI - halfBin) {//��������� � �������
						discriptor[binCount - 1] += gradient*(1 - partVariation);
						discriptor[0] += gradient*partVariation;
					}
					else {
						const auto numberFullBin = (int)floor(radAngle / fullBinAngle);
						discriptor[numberFullBin] += gradient*(1 - partVariation);
						if(numberFullBin==binCount-1)
							discriptor[numberFullBin-1] += gradient*partVariation;
						else
							discriptor[numberFullBin + 1] += gradient*partVariation;
					}
				}
			}
			//������� �������� � ������ �� ��� ��������
			double max=0, secondFromMax=-1;
			int maxIndex = 0, secondFromMaxIndex=0;
			for (int i = 0; i < discriptor.size(); i++) {
				if (i != 0) {
					if (max < discriptor[i]) {
						secondFromMax = max;
						secondFromMaxIndex = maxIndex;
						max = discriptor[i];
						maxIndex = i;
					} else 
						if (secondFromMax < discriptor[i])
						{
							secondFromMax = discriptor[i];
							secondFromMaxIndex = i;
						}
				}
				else {
					max = discriptor[i];
					maxIndex = i;
				}
			}
			const double h0 = discriptor[maxIndex];
			const double hm = discriptor[(maxIndex - 1 + binCount) % binCount];
			const double hp = discriptor[(maxIndex + 1 + binCount) % binCount];
			const double di = -0.5 * (hp - hm) / (hp + hm - 2 * h0);
			const double angle = 2 * PI * (maxIndex + di + 0.5) / binCount;
			result.emplace_back(ii.first, ii.second, angle);
			//������ �����
			if (secondFromMax > max*0.8) {
				const double h0 = discriptor[secondFromMaxIndex];
				const double hm = discriptor[(secondFromMaxIndex - 1 + binCount) % binCount];
				const double hp = discriptor[(secondFromMaxIndex + 1 + binCount) % binCount];
				const double di = -0.5 * (hp - hm) / (hp + hm - 2 * h0);
				const double angle = 2 * PI * (secondFromMaxIndex + di + 0.5) / binCount;
				result.emplace_back(ii.first, ii.second, angle);
			}
		}
		return result;
	}

	static tuple<int, int, double, vector<double>> MakeGOHFromImage(
const MatrixBiDimensional<double> &sobelXdiff,
		const MatrixBiDimensional<double> &sobelYdiff,
		pair<int, int> &pointOfInterest, 
		const int windowSize,
		const int subWindowSize,
		const double angle)
	{
		const auto binCount = 8;//���������� ������ �� 360 ��������
		const auto halfBinAngle = 2*PI / binCount / 2;
		const auto fullBinAngle = halfBinAngle * 2;
		const auto halfWindow = windowSize / 2;
		auto discriptor = vector<double>(subWindowSize*subWindowSize*binCount);
		for (int subwindowRow = -halfWindow; subwindowRow < halfWindow; subwindowRow++) {
			for (int subWindowColumn = -halfWindow; subWindowColumn < halfWindow; subWindowColumn++) {
				const int rotatedX = (int) (subWindowColumn*cos(angle) - subwindowRow*sin(angle))+halfWindow,
							 rotatedY =(int) (subWindowColumn*sin(angle) + subwindowRow*cos(angle))+halfWindow;
				if (rotatedX < 0 || rotatedX >= windowSize)
					continue;
				if (rotatedY < 0 || rotatedY >= windowSize)
					continue;
				const int binOffset = (rotatedY / subWindowSize * (windowSize / subWindowSize) + rotatedX / subWindowSize)*binCount;
				//��������� � ���������� �������� 
				const int pictureY = pointOfInterest.first + subwindowRow,
							 pictureX = pointOfInterest.second + subWindowColumn;

				if (pictureX < 0 || pictureX >= sobelXdiff.ColumnNumber())
					continue;
				if (pictureY < 0 || pictureY >= sobelXdiff.RowNumber())
					continue;

				const double tmpX = sobelXdiff.Get(pictureY, pictureX),
							 tmpY = sobelYdiff.Get(pictureY, pictureX);
				
				const double gradient = hypot(tmpX, tmpY)* exp(-(pow(subWindowColumn, 2) + pow(subwindowRow, 2)) / (2 * pow(2.0, 2))) / (2 * PI * pow(2.0, 2));

				auto radAngle = atan2(tmpY, tmpX) + PI - angle;
				if (radAngle > 2 * PI)
					radAngle -= 2 * PI;
				if (radAngle < 0)
					radAngle += 2 * PI;

				double part = radAngle / PI / 2 * binCount;//partsCount - ������ � �����������
				part = max(0.0, min(binCount - 0.001, part));

				const double partVariation = part - (int)part;
				int index = binOffset + (int)round(part) % binCount;
				discriptor[index] += gradient * (1 - partVariation);
				index = (int)(binOffset + part + 1) % binCount;
				discriptor[index] += gradient * partVariation;
			}
		}
		//������������
		auto summ = std::accumulate(discriptor.begin(), discriptor.end(), 0.0);
		for (int i = 0; i < discriptor.size(); i++)
				discriptor[i] = discriptor[i]/summ;
		for (int i = 0; i < discriptor.size(); i++)
			discriptor[i] = min(discriptor[i], 0.2);
		summ = std::accumulate(discriptor.begin(), discriptor.end(), 0.0);
		for (int i = 0; i < discriptor.size(); i++)
			discriptor[i] = discriptor[i] / summ;
		return tuple<int, int, double, vector<double>>(pointOfInterest.first, pointOfInterest.second,angle, discriptor);
	}
}