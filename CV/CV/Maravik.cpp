#include "stdafx.h"
#include "Maravik.h"
#include <algorithm>
#include <vector>
#include <tuple>

pair<const int, const int> Maravik::GetWindowSize() const
{
	return {m_windowSizeX, m_windowSizeY};
}

MatrixBiDimensional<double> Maravik::CalculateDifference(MatrixBiDimensional<double>& image) const
{
	const auto rows=image.RowNumber();
	const auto columns = image.ColumnNumber();
	vector<double> diffs = vector<double>(8);
	const auto halfWndHeight = m_windowSizeY / 2;
	const auto halfWndWidth = m_windowSizeX / 2;
	auto result = MatrixBiDimensional<double>(rows, columns);
	result.Set(0.0);
	for (auto i =  halfWndHeight+1 ; i < rows- halfWndHeight - 1 ; i++) { //����� ������������� ����� ���� �����������
		for (auto j = halfWndWidth+1; j < columns - halfWndWidth-1; j++) {
			double buff1 = 0.0,
					buff2 = 0.0;
			auto summ = 0.0;
			//left top
			for (auto k = -halfWndHeight; k <= halfWndHeight; k++) {
				for (auto m = -halfWndWidth; m <= halfWndWidth; m++) {
					buff1 = image.Get(i + k, j + m);
					buff2 = image.Get(i + k - 1, j + m - 1);
					summ += (buff1 - buff2)*(buff1 - buff2);
				}
			}
			diffs[0]=summ;
			summ = 0;
			//top
			for (auto k = -halfWndHeight; k <= halfWndHeight; k++) {
				for (auto m = -halfWndWidth; m <= halfWndWidth; m++) {
					buff1 = image.Get(i + k, j + m);
					buff2 = image.Get(i + k - 1 , j + m);
					summ += (buff1 - buff2)*(buff1 - buff2);
				}
			}
			diffs[1] = summ;
			summ = 0;
			//top right
			for (auto k = -halfWndHeight; k <= halfWndHeight; k++) {
				for (auto m = -halfWndWidth; m <= halfWndWidth; m++) {
					buff1 = image.Get(i + k, j + m);
					buff2 = image.Get(i + k - 1, j + m + 1);
					summ += (buff1 - buff2)*(buff1 - buff2);
				}
			}
			diffs[2] = summ;
			summ = 0;
			//right
			for (auto k = -halfWndHeight; k <= halfWndHeight; k++) {
				for (auto m = -halfWndWidth; m <= halfWndWidth; m++) {
					buff1 = image.Get(i + k, j + m);
					buff2 = image.Get(i + k, j + m + 1);
					summ += (buff1 - buff2)*(buff1 - buff2);
				}
			}
			diffs[3] = summ;
			summ = 0;
			//bottom right
			for (auto k = -halfWndHeight; k <= halfWndHeight; k++) {
				for (auto m = -halfWndWidth; m <= halfWndWidth; m++) {
					buff1 = image.Get(i + k, j + m);
					buff2 = image.Get(i + k+1, j + m +1);
					summ += (buff1 - buff2)*(buff1 - buff2);
				}
			}
			diffs[4] = summ;
			summ = 0;
			//bottom
			for (auto k = -halfWndHeight; k <= halfWndHeight; k++) {
				for (auto m = -halfWndWidth; m <= halfWndWidth; m++) {
					buff1 = image.Get(i + k, j + m);
					buff2 = image.Get(i + k + 1, j + m);
					summ += (buff1 - buff2)*(buff1 - buff2);
				}
			}
			diffs[5] = summ;
			summ = 0;
			//bottom left
			for (auto k = -halfWndHeight; k <= halfWndHeight; k++) {
				for (auto m = -halfWndWidth; m <= halfWndWidth; m++) {
					buff1 = image.Get(i + k, j + m);
					buff2 = image.Get(i + k + 1, j + m - 1);
					summ += (buff1 - buff2)*(buff1 - buff2);
				}
			}
			diffs[6] = summ;
			summ = 0;
			//left
			for (auto k = -halfWndHeight; k <= halfWndHeight; k++) {
				for (auto m = -halfWndWidth; m <= halfWndWidth; m++) {
					buff1 = image.Get(i + k, j + m);
					buff2 = image.Get(i + k, j + m - 1);
					summ += (buff1 - buff2)*(buff1 - buff2);
				}
			}
			diffs[7] = summ;
			result.Set(i- halfWndHeight, j-halfWndWidth, *std::min(diffs.begin(), diffs.end()));//-2 - ��� �����, ���� �� ����� ���� �������� ������� ����, �� ���� ������ ��� ����������
		}
	}
	return result;
}

Maravik::Maravik(const int windowSizeX,const int windowSizeY)
{
	m_windowSizeX = windowSizeX;
	m_windowSizeY = windowSizeY;
}

void Maravik::SetWindowsSize(const int sizeX, const int sizeY)
{
	assert(sizeX <= 0 || sizeY <= 0);
	assert(sizeX % 2 == 0 || sizeY % 2 == 0);
	m_windowSizeX = sizeX;
	m_windowSizeY = sizeY;
}

Maravik::~Maravik()
{
}
