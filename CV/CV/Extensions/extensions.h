#pragma once
#include "../stdafx.h"
#include <tuple>
#include <vector>
#include <assert.h>
#include "../MatrixBiDimensional.h"

using namespace System::Drawing;
using namespace std;
const auto sobelY = MatrixBiDimensional<double>({ { -1,-2,-1 },
													{ 0,0,0 },
													{ 1,2,1 } });
const auto sobelX = MatrixBiDimensional<double>({ { -1,0,1 },
{ -2,0,2 },
{ -1,0,1 } });

namespace Extensions {
	//преобразует изображение к серому виду
	static Bitmap ^ MakeShadowOfGray(Bitmap ^ sourceImage)
	{
		Bitmap ^clone = sourceImage->Clone(Rectangle(0, 0, sourceImage->Width, sourceImage->Height), sourceImage->PixelFormat);
		for (int i = 0; i < sourceImage->Height; i++)
		{
			for (int x = 0; x < sourceImage->Width; x++)
			{
				Color oc = sourceImage->GetPixel(x, i);
				Color nc = Color::FromArgb(oc.R, 0, 0);
				clone->SetPixel(i, x, nc);
			}
		}
		return clone;
	}

	//получает массив коэффициентов интенсивности для серого изображения
	static MatrixBiDimensional<double> GetGrayCoeff(Bitmap ^ sourceImage) {
		auto result = MatrixBiDimensional<double>(sourceImage->Height, sourceImage->Width);
		for (int i = 0; i < sourceImage->Height; i++) {
			for (int j = 0; j < sourceImage->Width; j++) {
				auto pixel = sourceImage->GetPixel(j, i);
				const auto grey = (unsigned char)((pixel.R * 0.3) + (pixel.G * 0.59) + (pixel.B * 0.11));
				result.Set(i, j, grey);
			}
		}
		return result;
	}

	//создаёт изображение из коэффициентов
	static Bitmap^ MakeImage(const MatrixBiDimensional<double> &coefficients) {
		Bitmap ^result = gcnew Bitmap(coefficients.ColumnNumber(), coefficients.RowNumber(), System::Drawing::Imaging::PixelFormat::Format32bppArgb);
		for (int i = 0; i < coefficients.RowNumber(); i++) {
			for (int j = 0; j < coefficients.ColumnNumber(); j++) {
				auto color = coefficients.Get(i, j);;
				result->SetPixel(j, i, Color::FromArgb(color, color, color));
			}
		}
		return result;
	}

	//
	static MatrixBiDimensional<double> DownSample(const MatrixBiDimensional<double> &image) {
		const auto rowNumber = image.RowNumber()/2;
		const auto columnNumber = image.ColumnNumber()/2;
		auto result = MatrixBiDimensional<double>(rowNumber, columnNumber);
		for (int i = 0; i < rowNumber; i++) {
			for (int j = 0; j < columnNumber; j++) {
				result.Set(i, j, image.Get(i * 2, j * 2));
			}
		}
		return result;
	}
	//нормирует значения матрицы, результат записывает в ту же матрицу
	static void Normalize(MatrixBiDimensional<double>& source, int maxValue=255) {
		const auto rowsNumber = source.RowNumber();
		const auto columnsNumber = source.ColumnNumber();
		const auto minMaxPair = source.MinMax();
		auto diff = minMaxPair.second - minMaxPair.first;
		for (int i = 0; i < rowsNumber; i++) {
			for (int j = 0; j < columnsNumber; j++) {
				auto aaa = (source.Get(i, j) - minMaxPair.first);
				source.Set(i, j, aaa / diff * maxValue);
			}
		}
	}

	static void DrawPoints(Bitmap ^image, const vector<pair<int, int>> &points, const int dx=0, const int dy=0)
	{
		auto graphForDrawing = Graphics::FromImage(image);
		auto brush = gcnew SolidBrush(Color::Red);
		for (auto ii : points) {
			graphForDrawing->FillRectangle(brush, ii.second + dx, ii.first + dy, 4, 4);
		}
	}

	static void SaveImage(Bitmap ^image, System::String ^fileName) {
		image->Save(fileName);
	}
};


