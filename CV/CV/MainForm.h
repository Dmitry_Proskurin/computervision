#pragma once
#include "Extensions/extensions.h"
#include "MatrixBiDimensional.h"
#include "Convolution.h"
#include "CopyEdges.h"
#include "Gauss.h"
#include "GaussPyramid.h"
#include "Maravik.h"
#include "GoH.h"
#include "Harris.h"
#include <math.h>
#include <memory>
#include <string>
#include "Course.h"

using namespace std;


namespace CV {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
		
		double sigma = 1;
	public:
		MainForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TabControl^  tabControl1;
	protected:
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::Button^  Calculate;

	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::Button^  Lab2Caclulation;
	private: System::Windows::Forms::NumericUpDown^  ScaleInOctave;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::NumericUpDown^  Lab2Sigma;
	private: System::Windows::Forms::TabPage^  tabPage3;
	private: System::Windows::Forms::Button^  Lab3Harris;
	private: System::Windows::Forms::Button^  Lab3Maravik;
	private: System::Windows::Forms::TabPage^  tabPage4;
	private: System::Windows::Forms::Button^  Lab4Button;
	private: System::Windows::Forms::TabPage^  tabPage5;
	private: System::Windows::Forms::Button^  Lab5_ButtonClick;
	private: System::Windows::Forms::TabPage^  tabPage6;
	private: System::Windows::Forms::Button^  Course;
	private: System::Windows::Forms::NumericUpDown^  RotateAngleUpDown;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::NumericUpDown^  FilterHarrisUpDown;
	private: System::Windows::Forms::NumericUpDown^  ANMSUpDown;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::CheckBox^  Matching;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::CheckBox^  Iteration;



	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->Calculate = (gcnew System::Windows::Forms::Button());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->Lab2Sigma = (gcnew System::Windows::Forms::NumericUpDown());
			this->ScaleInOctave = (gcnew System::Windows::Forms::NumericUpDown());
			this->Lab2Caclulation = (gcnew System::Windows::Forms::Button());
			this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
			this->Lab3Harris = (gcnew System::Windows::Forms::Button());
			this->Lab3Maravik = (gcnew System::Windows::Forms::Button());
			this->tabPage4 = (gcnew System::Windows::Forms::TabPage());
			this->Lab4Button = (gcnew System::Windows::Forms::Button());
			this->tabPage5 = (gcnew System::Windows::Forms::TabPage());
			this->Lab5_ButtonClick = (gcnew System::Windows::Forms::Button());
			this->tabPage6 = (gcnew System::Windows::Forms::TabPage());
			this->Matching = (gcnew System::Windows::Forms::CheckBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->FilterHarrisUpDown = (gcnew System::Windows::Forms::NumericUpDown());
			this->ANMSUpDown = (gcnew System::Windows::Forms::NumericUpDown());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->Course = (gcnew System::Windows::Forms::Button());
			this->RotateAngleUpDown = (gcnew System::Windows::Forms::NumericUpDown());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->Iteration = (gcnew System::Windows::Forms::CheckBox());
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->tabPage2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Lab2Sigma))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ScaleInOctave))->BeginInit();
			this->tabPage3->SuspendLayout();
			this->tabPage4->SuspendLayout();
			this->tabPage5->SuspendLayout();
			this->tabPage6->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->FilterHarrisUpDown))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ANMSUpDown))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->RotateAngleUpDown))->BeginInit();
			this->SuspendLayout();
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Controls->Add(this->tabPage3);
			this->tabControl1->Controls->Add(this->tabPage4);
			this->tabControl1->Controls->Add(this->tabPage5);
			this->tabControl1->Controls->Add(this->tabPage6);
			this->tabControl1->Location = System::Drawing::Point(12, 12);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(509, 120);
			this->tabControl1->TabIndex = 0;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->Calculate);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(501, 94);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Lab1";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// Calculate
			// 
			this->Calculate->Location = System::Drawing::Point(7, 7);
			this->Calculate->Name = L"Calculate";
			this->Calculate->Size = System::Drawing::Size(103, 31);
			this->Calculate->TabIndex = 0;
			this->Calculate->Text = L"Lab1Calculate";
			this->Calculate->UseVisualStyleBackColor = true;
			this->Calculate->Click += gcnew System::EventHandler(this, &MainForm::Calculate_Click);
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->label2);
			this->tabPage2->Controls->Add(this->label1);
			this->tabPage2->Controls->Add(this->Lab2Sigma);
			this->tabPage2->Controls->Add(this->ScaleInOctave);
			this->tabPage2->Controls->Add(this->Lab2Caclulation);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(501, 94);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Lab2";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(212, 8);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(39, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"�����";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(7, 12);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(112, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"��������� � ������";
			// 
			// Lab2Sigma
			// 
			this->Lab2Sigma->DecimalPlaces = 3;
			this->Lab2Sigma->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->Lab2Sigma->Location = System::Drawing::Point(257, 6);
			this->Lab2Sigma->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
			this->Lab2Sigma->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->Lab2Sigma->Name = L"Lab2Sigma";
			this->Lab2Sigma->Size = System::Drawing::Size(81, 20);
			this->Lab2Sigma->TabIndex = 1;
			this->Lab2Sigma->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			// 
			// ScaleInOctave
			// 
			this->ScaleInOctave->Location = System::Drawing::Point(125, 6);
			this->ScaleInOctave->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->ScaleInOctave->Name = L"ScaleInOctave";
			this->ScaleInOctave->Size = System::Drawing::Size(81, 20);
			this->ScaleInOctave->TabIndex = 1;
			this->ScaleInOctave->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			// 
			// Lab2Caclulation
			// 
			this->Lab2Caclulation->Location = System::Drawing::Point(124, 29);
			this->Lab2Caclulation->Name = L"Lab2Caclulation";
			this->Lab2Caclulation->Size = System::Drawing::Size(82, 30);
			this->Lab2Caclulation->TabIndex = 0;
			this->Lab2Caclulation->Text = L"����";
			this->Lab2Caclulation->UseVisualStyleBackColor = true;
			this->Lab2Caclulation->Click += gcnew System::EventHandler(this, &MainForm::Lab2Caclulation_Click);
			// 
			// tabPage3
			// 
			this->tabPage3->Controls->Add(this->Lab3Harris);
			this->tabPage3->Controls->Add(this->Lab3Maravik);
			this->tabPage3->Location = System::Drawing::Point(4, 22);
			this->tabPage3->Name = L"tabPage3";
			this->tabPage3->Size = System::Drawing::Size(501, 94);
			this->tabPage3->TabIndex = 2;
			this->tabPage3->Text = L"Lab3";
			this->tabPage3->UseVisualStyleBackColor = true;
			// 
			// Lab3Harris
			// 
			this->Lab3Harris->Location = System::Drawing::Point(271, 23);
			this->Lab3Harris->Name = L"Lab3Harris";
			this->Lab3Harris->Size = System::Drawing::Size(75, 23);
			this->Lab3Harris->TabIndex = 1;
			this->Lab3Harris->Text = L"������";
			this->Lab3Harris->UseVisualStyleBackColor = true;
			this->Lab3Harris->Click += gcnew System::EventHandler(this, &MainForm::Lab3Harris_Click);
			// 
			// Lab3Maravik
			// 
			this->Lab3Maravik->Location = System::Drawing::Point(162, 23);
			this->Lab3Maravik->Name = L"Lab3Maravik";
			this->Lab3Maravik->Size = System::Drawing::Size(75, 23);
			this->Lab3Maravik->TabIndex = 0;
			this->Lab3Maravik->Text = L"�������";
			this->Lab3Maravik->UseVisualStyleBackColor = true;
			this->Lab3Maravik->Click += gcnew System::EventHandler(this, &MainForm::Lab3Maravik_Click);
			// 
			// tabPage4
			// 
			this->tabPage4->Controls->Add(this->Lab4Button);
			this->tabPage4->Location = System::Drawing::Point(4, 22);
			this->tabPage4->Name = L"tabPage4";
			this->tabPage4->Size = System::Drawing::Size(501, 94);
			this->tabPage4->TabIndex = 3;
			this->tabPage4->Text = L"Lab4";
			this->tabPage4->UseVisualStyleBackColor = true;
			// 
			// Lab4Button
			// 
			this->Lab4Button->Location = System::Drawing::Point(166, 12);
			this->Lab4Button->Name = L"Lab4Button";
			this->Lab4Button->Size = System::Drawing::Size(118, 35);
			this->Lab4Button->TabIndex = 0;
			this->Lab4Button->Text = L"button1";
			this->Lab4Button->UseVisualStyleBackColor = true;
			this->Lab4Button->Click += gcnew System::EventHandler(this, &MainForm::Lab4Button_Click);
			// 
			// tabPage5
			// 
			this->tabPage5->Controls->Add(this->Lab5_ButtonClick);
			this->tabPage5->Location = System::Drawing::Point(4, 22);
			this->tabPage5->Name = L"tabPage5";
			this->tabPage5->Size = System::Drawing::Size(501, 94);
			this->tabPage5->TabIndex = 4;
			this->tabPage5->Text = L"Lab5";
			this->tabPage5->UseVisualStyleBackColor = true;
			// 
			// Lab5_ButtonClick
			// 
			this->Lab5_ButtonClick->Location = System::Drawing::Point(159, 15);
			this->Lab5_ButtonClick->Name = L"Lab5_ButtonClick";
			this->Lab5_ButtonClick->Size = System::Drawing::Size(125, 29);
			this->Lab5_ButtonClick->TabIndex = 0;
			this->Lab5_ButtonClick->Text = L"Do something";
			this->Lab5_ButtonClick->UseVisualStyleBackColor = true;
			this->Lab5_ButtonClick->Click += gcnew System::EventHandler(this, &MainForm::Lab5_ButtonClick_Click);
			// 
			// tabPage6
			// 
			this->tabPage6->Controls->Add(this->Iteration);
			this->tabPage6->Controls->Add(this->Matching);
			this->tabPage6->Controls->Add(this->label5);
			this->tabPage6->Controls->Add(this->FilterHarrisUpDown);
			this->tabPage6->Controls->Add(this->ANMSUpDown);
			this->tabPage6->Controls->Add(this->label4);
			this->tabPage6->Controls->Add(this->Course);
			this->tabPage6->Controls->Add(this->RotateAngleUpDown);
			this->tabPage6->Controls->Add(this->label3);
			this->tabPage6->Location = System::Drawing::Point(4, 22);
			this->tabPage6->Name = L"tabPage6";
			this->tabPage6->Size = System::Drawing::Size(501, 94);
			this->tabPage6->TabIndex = 5;
			this->tabPage6->Text = L"��������";
			this->tabPage6->UseVisualStyleBackColor = true;
			// 
			// Matching
			// 
			this->Matching->AutoSize = true;
			this->Matching->Location = System::Drawing::Point(256, 11);
			this->Matching->Name = L"Matching";
			this->Matching->Size = System::Drawing::Size(166, 17);
			this->Matching->TabIndex = 6;
			this->Matching->Text = L"���������� �������������";
			this->Matching->UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(6, 63);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(62, 13);
			this->label5->TabIndex = 5;
			this->label5->Text = L"������ UP";
			// 
			// FilterHarrisUpDown
			// 
			this->FilterHarrisUpDown->Location = System::Drawing::Point(116, 60);
			this->FilterHarrisUpDown->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000000, 0, 0, 0 });
			this->FilterHarrisUpDown->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->FilterHarrisUpDown->Name = L"FilterHarrisUpDown";
			this->FilterHarrisUpDown->Size = System::Drawing::Size(120, 20);
			this->FilterHarrisUpDown->TabIndex = 4;
			this->FilterHarrisUpDown->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 30000, 0, 0, 0 });
			// 
			// ANMSUpDown
			// 
			this->ANMSUpDown->Location = System::Drawing::Point(116, 34);
			this->ANMSUpDown->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000000, 0, 0, 0 });
			this->ANMSUpDown->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->ANMSUpDown->Name = L"ANMSUpDown";
			this->ANMSUpDown->Size = System::Drawing::Size(120, 20);
			this->ANMSUpDown->TabIndex = 4;
			this->ANMSUpDown->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 100, 0, 0, 0 });
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 39);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(38, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"ANMS";
			// 
			// Course
			// 
			this->Course->Location = System::Drawing::Point(256, 53);
			this->Course->Name = L"Course";
			this->Course->Size = System::Drawing::Size(89, 23);
			this->Course->TabIndex = 2;
			this->Course->Text = L"���������";
			this->Course->UseVisualStyleBackColor = true;
			this->Course->Click += gcnew System::EventHandler(this, &MainForm::Course_Click);
			// 
			// RotateAngleUpDown
			// 
			this->RotateAngleUpDown->Location = System::Drawing::Point(116, 8);
			this->RotateAngleUpDown->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 360, 0, 0, 0 });
			this->RotateAngleUpDown->Name = L"RotateAngleUpDown";
			this->RotateAngleUpDown->Size = System::Drawing::Size(120, 20);
			this->RotateAngleUpDown->TabIndex = 1;
			this->RotateAngleUpDown->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(3, 11);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(107, 13);
			this->label3->TabIndex = 0;
			this->label3->Text = L"���� ��������(���)";
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			this->openFileDialog1->Filter = L"����������� (*.png)|*.png";
			// 
			// Iteration
			// 
			this->Iteration->AutoSize = true;
			this->Iteration->Location = System::Drawing::Point(256, 30);
			this->Iteration->Name = L"Iteration";
			this->Iteration->Size = System::Drawing::Size(93, 17);
			this->Iteration->TabIndex = 6;
			this->Iteration->Text = L"�����������";
			this->Iteration->UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(530, 138);
			this->Controls->Add(this->tabControl1);
			this->Name = L"MainForm";
			this->Text = L"MainForm";
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Lab2Sigma))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ScaleInOctave))->EndInit();
			this->tabPage3->ResumeLayout(false);
			this->tabPage4->ResumeLayout(false);
			this->tabPage5->ResumeLayout(false);
			this->tabPage6->ResumeLayout(false);
			this->tabPage6->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->FilterHarrisUpDown))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ANMSUpDown))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->RotateAngleUpDown))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		shared_ptr<MatrixBiDimensional<double>> m_oldImage;
	}
private: System::Void Lab2Caclulation_Click(System::Object^  sender, System::EventArgs^  e) {
	Image ^img = Image::FromFile(gcnew String(".\\testImages\\Lab2\\test.jpg"));
	auto bitmap = gcnew Bitmap(img);
	auto grays = Extensions::GetGrayCoeff(bitmap);
	auto edgeResolver = shared_ptr<IEdgeBehaviour>(new CopyEdges());
	auto transformation = new Convolution(edgeResolver);
	auto b = GaussPyramid::CalculatePyramid(grays, 
											(double)Lab2Sigma->Value, 
											(double)ScaleInOctave->Value, 
											4);
	int i = 0;
	for (auto ii : b) {
		Extensions::SaveImage(Extensions::MakeImage(ii.first), String::Format("{1}_Image{0:N3}.jpg", ii.second,i++));
	}
	MessageBox::Show("������");
}

private: System::Void Lab3Maravik_Click(System::Object^  sender, System::EventArgs^  e) {
	Image ^img = Image::FromFile(gcnew String(".\\testImages\\Lab3\\test.jpg"));
	auto bitmap = gcnew Bitmap(img);
	auto grays = Extensions::GetGrayCoeff(bitmap);
	Extensions::SaveImage(Extensions::MakeImage(grays), "gray.jpg");
	auto maravik = Maravik(5, 5);
	auto maravikCoeff=maravik.CalculateDifference(grays);
	Extensions::Normalize(maravikCoeff);
	auto maravikPoints=Harris::FilterPoints(maravikCoeff,4, 5,5);
	Harris::ANMS(maravikPoints, 100, grays.RowNumber(), grays.ColumnNumber());
	vector<pair<int, int>> withoutCoeff = vector<pair<int, int>>();
	for (auto ii : maravikPoints)
		if(!isnan(get<0>(ii)))
		withoutCoeff.emplace_back(get<1>(ii), get<2>(ii));
	Extensions::DrawPoints(bitmap, withoutCoeff);
	Extensions::SaveImage(bitmap, "maravikTest2.jpg");
}
private: System::Void Lab3Harris_Click(System::Object^  sender, System::EventArgs^  e) {
	Image ^img = Image::FromFile(gcnew String(".\\testImages\\Lab3\\test.png"));
	auto bitmap = gcnew Bitmap(img);
	auto grays = Extensions::GetGrayCoeff(bitmap);
	auto harris = Harris(5, 5);
	auto points = harris.CalculateHarris(grays);
	//Extensions::Normalize(points);
	auto filteredPoints = Harris::FilterPoints(points, 30000, 5, 5);
	Console::WriteLine("After filtration.");
	for (auto ii : filteredPoints) {
		Console::WriteLine(get<0>(ii));
	}
	Harris::ANMS(filteredPoints, 100, grays.RowNumber(), grays.ColumnNumber());
	
	vector<pair<int, int>> withoutCoeff = vector<pair<int, int>>();
	for (auto ii : filteredPoints)
		if(!isnan(get<0>(ii)))
		withoutCoeff.emplace_back(get<1>(ii), get<2>(ii));
	Extensions::DrawPoints(bitmap, withoutCoeff);
	Extensions::SaveImage(bitmap, "harrisTest1.png");
	Console::WriteLine("Ready");
}
private: System::Void Lab4Button_Click(System::Object^  sender, System::EventArgs^  e) {
	Image ^img1 = Image::FromFile(gcnew String(".\\testImages\\Lab4\\test1.png"));
	auto bitmap1 = gcnew Bitmap(img1);
	auto grays1 = Extensions::GetGrayCoeff(bitmap1);
	Image ^img2 = Image::FromFile(gcnew String(".\\testImages\\Lab4\\test2.png"));
	auto bitmap2 = gcnew Bitmap(img2);
	auto grays2 = Extensions::GetGrayCoeff(bitmap2);
	////////////////////////////////////////////////
	//1 image
	auto harris = Harris(7, 7);
	auto points1 = harris.CalculateHarris(grays1);
	//Extensions::Normalize(points1);
	auto filteredPoints1 = Harris::FilterPoints(points1, 30000, 7, 7);
	Harris::ANMS(filteredPoints1, 40, grays1.RowNumber(), grays1.ColumnNumber());
	auto withoutCoeff1 = vector<pair<int, int>>();
	for (auto ii : filteredPoints1)
		if (!isnan(get<0>(ii)))
			withoutCoeff1.emplace_back(get<1>(ii), get<2>(ii));
	//2 image
	//{
		auto points2 = harris.CalculateHarris(grays2);
		//Extensions::Normalize(points2);
		auto filteredPoints2 = Harris::FilterPoints(points2, 30000, 7, 7);
		Harris::ANMS(filteredPoints2, 40, grays2.RowNumber(), grays2.ColumnNumber());
		auto withoutCoeff2 = vector<pair<int, int>>();
		for (auto ii : filteredPoints2)
			if (!isnan(get<0>(ii)))
				withoutCoeff2.emplace_back(get<1>(ii), get<2>(ii));
	//}
	//���� �� ������� �������� ��� ����������� � �����������
	const int xTransform = bitmap1->Width + 100;
	Bitmap ^bb = gcnew Bitmap(bitmap1->Width + 100 + bitmap2->Width, bitmap1->Height);
	auto graphForDrawing = Graphics::FromImage(bb);
	auto brush = gcnew SolidBrush(Color::Red);
	graphForDrawing->DrawImage(bitmap1, 0, 0);
	graphForDrawing->DrawImage(bitmap2, xTransform, 0);
	Extensions::DrawPoints(bb, withoutCoeff1);
	Extensions::DrawPoints(bb, withoutCoeff2, xTransform);
	auto edgeResolver = new CopyEdges();
	auto edgeResolver1 = shared_ptr<IEdgeBehaviour>(edgeResolver);
	auto transformation = new Convolution(edgeResolver1);
	const auto sobelXgrays1 = transformation->Transform(grays1, sobelX);
	const auto sobelYgrays1 = transformation->Transform(grays1, sobelY);
	const auto sobelXgrays2 = transformation->Transform(grays2, sobelX);
	const auto sobelYgrays2 = transformation->Transform(grays2, sobelY);
	//��������� �����
	auto descr1 = vector < tuple<int, int, double, vector<double>>>();
	auto descr2 = vector < tuple<int, int, double, vector<double>>>();
	for (auto ii : withoutCoeff1) {
		descr1.push_back(GoH::MakeGOHFromImage(sobelXgrays1, sobelYgrays1, ii, 16, 4, 0));
	}
	for (auto ii : withoutCoeff2) {
		descr2.push_back(GoH::MakeGOHFromImage(sobelXgrays2, sobelYgrays2, ii, 16, 4, 0));
	}
	for (auto ii : descr1) 
	{
		double minDistance = -1;
		int iMin, jMin;
		for (auto jj : descr2) 
		{
				if (minDistance < 0)
				{
					minDistance = GoH::DistanceBetweenDescriptors(ii,jj);
					iMin = get<0>(jj);
					jMin = get<1>(jj);
				}
				else 
				{
					double newDist = GoH::DistanceBetweenDescriptors(ii,jj);
					if (minDistance > newDist) {
						minDistance = newDist;
						iMin = get<0>(jj);
						jMin = get<1>(jj);
					}
				}
		}
		Console::WriteLine(minDistance);
		if(minDistance < 1)
			graphForDrawing->DrawLine(gcnew Pen(Color::Red), get<1>(ii), get<0>(ii), jMin + xTransform, iMin);
	}
	Extensions::SaveImage(bb, "test4.jpg");
}
private: System::Void Lab5_ButtonClick_Click(System::Object^  sender, System::EventArgs^  e) {
	auto brush = gcnew SolidBrush(Color::Red);
	Image ^img1 = Image::FromFile(gcnew String(".\\testImages\\Lab5\\test1.png"));
	auto bitmap1 = gcnew Bitmap(img1);
	Image ^img2 = Image::FromFile(gcnew String(".\\testImages\\Lab5\\test2.png"));
	auto bitmap2 = gcnew Bitmap(img2);
	Extensions::SaveImage(bitmap1, "1_points.png");
	Extensions::SaveImage(bitmap2, "2_points.png");
	//���� �� ������� �������� ��� ����������� � �����������
	const int xTransform = bitmap1->Width + 100;
	Bitmap ^bb = gcnew Bitmap(bitmap1->Width + 100 + bitmap2->Width, bitmap1->Height>bitmap2->Height?bitmap1->Height:bitmap2->Height);
	auto graphForDrawing = Graphics::FromImage(bb);
	
	graphForDrawing->DrawImage(bitmap1, 0, 0);
	graphForDrawing->DrawImage(bitmap2, xTransform, 0);
	//��������� �����
	auto descr1 = GetPointsWithDirection(gcnew String(".\\testImages\\Lab5\\test1.png"), gcnew String("1_points.png"), 1);
	auto descr2 = GetPointsWithDirection(gcnew String(".\\testImages\\Lab5\\test2.png"), gcnew String("2_points.png"), 0);
	Console::WriteLine("����������:");
	Course::MatchDescriptors(descr1, descr2, graphForDrawing, xTransform);
	Extensions::SaveImage(bb, "test5.jpg");
}
protected: vector<tuple<int, int, double, vector<double>>> MainForm::GetPointsWithDirection(System::String ^fileName, System::String ^fileNamePoints, bool max) {
	Image ^img = Image::FromFile(fileName);
	auto bitmap = gcnew Bitmap(img);
	auto grays = Extensions::GetGrayCoeff(bitmap);
	////////////////////////////////////////////////
	auto harris = Harris(7, 7);
	auto points = harris.CalculateHarris(grays);
	//Extensions::Normalize(points);
	auto filteredPoints1 = Harris::FilterPoints(points, 30000, 7, 7);
	Harris::ANMS(filteredPoints1, 50, grays.RowNumber(), grays.ColumnNumber());
	auto withoutCoeff = vector<pair<int, int>>();
	for (auto ii : filteredPoints1)
		if (!isnan(get<0>(ii)))
			withoutCoeff.emplace_back(get<1>(ii), get<2>(ii));
	Extensions::DrawPoints(bitmap, withoutCoeff);
			 
	Extensions::SaveImage(bitmap, fileNamePoints);
	auto withAngle = GoH::CalculateDirection(grays, withoutCoeff, 16, 1.0, 36,max);
	Console::WriteLine(fileNamePoints);
	for (auto ii : withAngle)
	{
		Console::WriteLine(get<0>(ii) + " " +get<1>(ii) + " " + get<2>(ii));
	}
	auto descr = vector<tuple<int, int,double, vector<double>>>();
	auto edgeResolver = new CopyEdges();
	auto edgeResolver1 = shared_ptr<IEdgeBehaviour>(edgeResolver);
	auto transformation = new Convolution(edgeResolver1);
	const auto sobelXgrays = transformation->Transform(grays, sobelX);
	const auto sobelYgrays = transformation->Transform(grays, sobelY);
	for (auto ii : withAngle) {
		descr.push_back(GoH::MakeGOHFromImage(sobelXgrays, sobelYgrays, pair<int, int>(get<0>(ii), get<1>(ii)), 16, 4, get<2>(ii)));
	}
	return descr;
}

private: System::Void Course_Click(System::Object^  sender, System::EventArgs^  e) {
	auto brush = gcnew SolidBrush(Color::Red);
	double rotateAngle =(double)RotateAngleUpDown->Value;//rad
	const auto anmsValue = (int)ANMSUpDown->Value;
	const auto harrisUp = (int)FilterHarrisUpDown->Value;
	const auto match = Matching->Checked;
	if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		if (openFileDialog1->CheckPathExists)
		{
			Course::MainCourse(anmsValue, harrisUp, match, rotateAngle,openFileDialog1->FileName, Iteration->Checked?360:rotateAngle+1);
		}
	}
}

private: System::Void Calculate_Click(System::Object^  sender, System::EventArgs^  e) {
	Image ^img = Image::FromFile(gcnew String(".\\testImages\\Lab1\\test.png"));
	auto bitmap = gcnew Bitmap(img);
	auto grays = Extensions::GetGrayCoeff(bitmap);
	auto edgeResolver = shared_ptr<IEdgeBehaviour>(new CopyEdges());
	auto transformation = new Convolution(edgeResolver);
	auto strings = gcnew System::Collections::Generic::List<String^>(500);
	
	auto a = transformation->Transform(grays, sobelX);
	auto b = transformation->Transform(grays, sobelY);
	auto preResult = MatrixBiDimensional<double>(a.RowNumber(), a.ColumnNumber());
	for (int i = 0; i < preResult.RowNumber(); i++) {
		for (int j = 0; j < preResult.ColumnNumber(); j++) {
			auto xDeriviative = a.Get(i, j);
			auto yDeriviative = b.Get(i, j);
			preResult.Set(i, j, Math::Sqrt(xDeriviative*xDeriviative + yDeriviative*yDeriviative));
		}
	}
	Extensions::Normalize(preResult);
	Bitmap ^result = Extensions::MakeImage(preResult);
	Extensions::SaveImage(result, "Sobel.jpg");
	MessageBox::Show("������");
}
};
}
